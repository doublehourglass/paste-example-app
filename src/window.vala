/* window.vala
 *
 * Copyright 2019 doublehourglass
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Gtk;
using Gdk;

namespace PasteExample {
[GtkTemplate (ui = "/me/dhg/paste_example/window.ui")]
public class Window : Gtk.ApplicationWindow {
[GtkChild]
TextBuffer text_buffer;
[GtkChild]
TextView text_viewer;

Clipboard clb;
static construct {}

construct {
	clb = get_clipboard(SELECTION_CLIPBOARD);
	}

public Window (Gtk.Application app) {
	Object (application: app);
	text_buffer.paste_done.connect(pd);
	text_viewer.set_wrap_mode(WrapMode.WORD);
	text_viewer.set_editable(false);
	// just let's list active targets
	Atom[] tgs = {};
	clb.wait_for_targets(out tgs);
	foreach ( Atom tg in tgs ) {
		message("TARGET: %s", tg.name());
		}
	}


public void pd (Clipboard clipboard) {
	clb.request_contents(Atom.intern("text/html", false), get_html);
	}

public void get_html (Clipboard clipboard, SelectionData sd) {
	if ( sd.get_length()>0 ) {
		text_buffer.set_text((string)( sd.get_data()));
		} else {
		text_buffer.set_text("NOT HTML", -1);
		}
	}

}


}
